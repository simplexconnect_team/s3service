package com.simplex.s3.controller;

import java.io.ByteArrayOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.simplex.s3.service.S3Service;

@RestController
public class S3Controller {
	
	@Autowired
	private S3Service s3Service;
		
	@GetMapping("/file/{folderName}/{keyName:.+}")
	public ResponseEntity<byte[]> downloadFile(@PathVariable String folderName, @PathVariable String keyName) {
		ByteArrayOutputStream downloadInputStream = s3Service.downloadFile(folderName, keyName);
	
		return ResponseEntity.ok()
			//.contentType(contentType(keyName))
			//.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + keyName + "\"")
			.body(downloadInputStream.toByteArray());	
	}
	
	/*private MediaType contentType(String keyName) {
		String[] arr = keyName.split("\\.");
		String type = arr[arr.length-1];
		switch(type) {
			case "txt": 
				return MediaType.TEXT_PLAIN;
			case "png": 
				return MediaType.IMAGE_PNG;
			case "jpg": 
				return MediaType.IMAGE_JPEG;
			case "jpeg": 
				return MediaType.IMAGE_JPEG;
			default: 
				return MediaType.APPLICATION_OCTET_STREAM;
		}
	}*/
	
	@PostMapping(value="/file/upload", consumes="multipart/form-data")
	public ResponseEntity<String> uploadMultipartFile(@RequestParam("folderName") String folderName, 
		@RequestParam("keyName") String keyName, @RequestParam("uploadFile") MultipartFile file) {
		s3Service.uploadFile(folderName, keyName, file);
		
		return ResponseEntity.ok()
			.body("Upload Successfully. -> KeyName = " + keyName); 
	}
	
}