package com.simplex.s3.service;

import java.io.ByteArrayOutputStream;

import org.springframework.web.multipart.MultipartFile;

public interface S3Service {
	
	ByteArrayOutputStream downloadFile(String folderName, String keyName);
	
	void uploadFile(String folderName, String keyName, MultipartFile file);
	
}