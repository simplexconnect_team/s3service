package com.simplex.s3.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

@Service("s3Service")
public class S3ServiceImpl implements S3Service {
	
	private Logger logger = LoggerFactory.getLogger(S3ServiceImpl.class);
	
	private String SUFFIX = "/";
	
	@Autowired
	private AmazonS3 s3Client;
	
	@Value("${s3.bucket}")
	private String bucketName;
	
	@Override
	public ByteArrayOutputStream downloadFile(String folderName, String fileName) {
		try {
			logger.info("Downloading an object called " + fileName 
				+  " from the " + bucketName + " bucket under " + folderName + " folder");
			String path = bucketName + SUFFIX + folderName;
            S3Object s3object = s3Client.getObject(new GetObjectRequest(path, fileName));
            
            InputStream is = s3object.getObjectContent();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int len;
            byte[] buffer = new byte[4096];
            while((len = is.read(buffer, 0, buffer.length)) != -1) {
                baos.write(buffer, 0, len);
            }
            logger.info("------ File Downloaded ------");
            return baos;
		} catch(IOException ioe) {
    		logger.error("IOException:" + ioe.getMessage());
        } catch(AmazonServiceException ase) {
        	logger.error("Caught an AmazonServiceException from GET requests. Rejected reasons,");
			logger.error("Error Message:" + ase.getMessage());
			logger.error("HTTP Status Code:" + ase.getStatusCode());
			logger.error("AWS Error Code:" + ase.getErrorCode());
			logger.error("Error Type:" + ase.getErrorType());
			logger.error("Request Id:" + ase.getRequestId());
			throw ase;
        } catch(AmazonClientException ace) {
        	logger.error("Caught an AmazonClientException");
            logger.error("Error Message:" + ace.getMessage());
            throw ace;
        }
		return null;
	}

	@Override
	public void uploadFile(String folderName, String keyName, MultipartFile file) {
		try {
			logger.info("Uploading an object called " + keyName 
				+  " into the " + bucketName + " bucket under " + folderName + " folder");
			String path = bucketName + SUFFIX + folderName;
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(file.getSize());
			s3Client.putObject(new PutObjectRequest(path, keyName, file.getInputStream(), metadata)
		        .withCannedAcl(CannedAccessControlList.PublicRead));
	        logger.info("------ File " + keyName + " Uploaded ------");
		} catch(IOException ioe) {
			logger.error("IOException:" + ioe.getMessage());
		} catch(AmazonServiceException ase) {
			logger.error("Caught an AmazonServiceException from PUT requests. Rejected reasons,");
			logger.error("Error Message:" + ase.getMessage());
			logger.error("HTTP Status Code:" + ase.getStatusCode());
			logger.error("AWS Error Code:" + ase.getErrorCode());
			logger.error("Error Type:" + ase.getErrorType());
			logger.error("Request Id:" + ase.getRequestId());
        } catch(AmazonClientException ace) {
            logger.error("Caught an AmazonClientException");
            logger.error("Error Message: " + ace.getMessage());
        }
	}

}